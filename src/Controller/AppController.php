<?php

namespace App\Controller;

use App\Form\ContactType;
use Swift_Mailer;
use Swift_Message;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class AppController extends AbstractController
{

    /** @Route("/", name="index") */
    public function index(Request $request, Swift_Mailer $mailer)
    {

        $form = $this->createForm(ContactType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contactFormData = $form->getData();

            $message = (new Swift_Message($contactFormData['fromEmail']))
                ->setFrom('', '')
                ->setReplyTo($contactFormData['fromEmail'])
                ->setTo('')
                ->setBody(
                    $this->renderView('emails/contact.html.twig', $contactFormData),
                    'text/html'
                )
            ;

            $mailer->send($message);
            $this->addFlash('success', 'Nachricht wurde gesendet.');
            $form = $this->createForm(ContactType::class);
        }

        return $this->render('pages/index.html.twig', [
            'email_form' => $form->createView(),
        ]);
    }

    /** @Route("/impressum", name="impressum") */
    public function impressum()
    {
        return $this->render('pages/impressum.html.twig');
    }

    /** @Route("/datenschutz", name="datenschutz") */
    public function datenschutz()
    {
        return $this->render('pages/datenschutz.html.twig');
    }
}