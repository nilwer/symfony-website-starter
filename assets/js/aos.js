const $ = require('jquery');
window.$ = window.jQuery = $;

const AOS = require('aos');
import 'aos/dist/aos.css';

$(document).ready(function($) {

    "use strict";

    AOS.init({
        duration: 800,
        easing: 'slide',
        once: false
    });
});